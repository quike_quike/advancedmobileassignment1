//
//  CategoriesTableViewController.swift
//  abc
//
//  Created by Carlos Alavez on 2017-05-04.
//  Copyright © 2017 Carlos Alavez. All rights reserved.
//

import UIKit
import Alamofire

class CategoriesTableViewController: UITableViewController {
    
    var ids = [String]()
    var selectedRow = Int()
    @IBOutlet var table: UITableView!
    
    var categoriesList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCosas()
        
    }
    
    func loadCosas(){
        let urlString = "http://www.bestbuy.ca/api/v2/json/category/departments?lang=en&format=json"
        
        Alamofire.request(.GET, urlString).responseJSON{ response in switch response.result {
        case .Success(let JSON):
            
            let response = JSON as! NSDictionary
            
            let categories = response.objectForKey("subCategories")!.count
            var category = 0
            
            while(category<categories-1){
                let lacosa = response.objectForKey("subCategories")![category];
                
                self.categoriesList.append(((lacosa.objectForKey("name")) as? String) ?? "")
                self.ids.append((lacosa.objectForKey("id") as? String) ?? "")
                
                self.table.reloadData()
                
                category=category+1
            }
            
            
        case .Failure(let error):
            print("Request failed with error: \(error)")
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Data Source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoriesList.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.textLabel?.text = self.categoriesList[indexPath.row]
        return cell
    }
    
    //*
    //MARK: - Delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedRow = indexPath.row
        self.performSegueWithIdentifier("sub", sender:self)
    }
    //*/
    //*
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        let id = ids[selectedRow]
        
        let destinationVC = segue.destinationViewController as! SubCategoriesTableViewController
        destinationVC.id = id
    }
    //*/

}
