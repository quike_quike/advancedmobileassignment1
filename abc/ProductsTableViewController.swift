//
//  ProductsTableViewController.swift
//  abc
//
//  Created by Carlos Alavez on 2017-05-04.
//  Copyright © 2017 Carlos Alavez. All rights reserved.
//
import Alamofire
import UIKit

class ProductsTableViewController: UITableViewController {
    
    //var selectedRow = Int()
    var id = String()
    var names = [String]()
    var ids = [String]()
    

    @IBOutlet var table: UITableView!
    
    var cosas = [String]()
    var categoriesList = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        print("LOL")
        loadCosas()
        print("LMAO")
        
    }
    
    
    
    func loadCosas(){
        let urlString = "http://www.bestbuy.ca/api/v2/json/search?categoryid=\(self.id)&lang=en"
        
        Alamofire.request(.GET, urlString).responseJSON{ response in switch response.result {
        case .Success(let JSON):
            
            let response = JSON as! NSDictionary
            
            let categories = response.objectForKey("products")!.count
            var category = 0
            
            while(category<categories){
                
                let lacosa = response.objectForKey("products")![category];
                
                self.categoriesList.append(((lacosa.objectForKey("name")) as? String) ?? "")
                //self.ids.append((lacosa.objectForKey("id") as? String) ?? "")
                
                self.table.reloadData()
                category=category+1
            }
        case .Failure(let error):
            print("Request failed with error: \(error)")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Data Source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoriesList.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.textLabel?.text = self.categoriesList[indexPath.row]
        return cell
    }

}
