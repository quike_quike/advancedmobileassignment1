//
//  ViewController.swift
//  abc
//
//  Created by Carlos Alavez on 2017-04-03.
//  Copyright © 2017 Carlos Alavez. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let urlString = "http://www.bestbuy.ca/api/v2/json/category/departments?lang=en&format=json"
        
        Alamofire.request(.GET, urlString).responseJSON{ response in switch response.result {
        case .Success(let JSON):
            //print("Success with JSON: \(JSON)")
            
            let response = JSON as! NSDictionary
            
            //example if there is an id
            let categories = response.objectForKey("subCategories")!.count
            
            print(categories)
            print(response.objectForKey("subCategories")![25])
            
            
        case .Failure(let error):
            print("Request failed with error: \(error)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

